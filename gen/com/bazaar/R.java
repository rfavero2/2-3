/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.bazaar;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int AliceBlue=0x7f040030;
        public static final int AntiqueWhite=0x7f040026;
        public static final int Aqua=0x7f040080;
        public static final int Aquamarine=0x7f040065;
        public static final int Azure=0x7f04002e;
        public static final int Beige=0x7f04002b;
        public static final int Bisque=0x7f04000e;
        public static final int Black=0x7f04008f;
        public static final int BlanchedAlmond=0x7f04000c;
        public static final int Blue=0x7f04008b;
        public static final int BlueViolet=0x7f04005e;
        public static final int Brown=0x7f040052;
        public static final int BurlyWood=0x7f040038;
        public static final int CadetBlue=0x7f040070;
        public static final int Chartreuse=0x7f040066;
        public static final int Chocolate=0x7f040042;
        public static final int Coral=0x7f040018;
        public static final int CornflowerBlue=0x7f04006f;
        public static final int Cornsilk=0x7f040008;
        public static final int Crimson=0x7f04003b;
        public static final int Cyan=0x7f040081;
        public static final int DarkBlue=0x7f04008d;
        public static final int DarkCyan=0x7f040087;
        public static final int DarkGoldenrod=0x7f04004a;
        public static final int DarkGray=0x7f040051;
        public static final int DarkGreen=0x7f04008a;
        public static final int DarkKhaki=0x7f040047;
        public static final int DarkMagenta=0x7f04005c;
        public static final int DarkOliveGreen=0x7f040071;
        public static final int DarkOrange=0x7f040017;
        public static final int DarkOrchid=0x7f040055;
        public static final int DarkRed=0x7f04005d;
        public static final int DarkSalmon=0x7f040035;
        public static final int DarkSeaGreen=0x7f04005a;
        public static final int DarkSlateBlue=0x7f040074;
        public static final int DarkSlateGray=0x7f04007a;
        public static final int DarkTurquoise=0x7f040085;
        public static final int DarkViolet=0x7f040057;
        public static final int DeepPink=0x7f04001c;
        public static final int DeepSkyBlue=0x7f040086;
        public static final int DimGray=0x7f04006d;
        public static final int DodgerBlue=0x7f04007e;
        public static final int FireBrick=0x7f04004b;
        public static final int FloralWhite=0x7f040006;
        public static final int ForestGreen=0x7f04007c;
        public static final int Fuchsia=0x7f04001d;
        public static final int Gainsboro=0x7f04003a;
        public static final int GhostWhite=0x7f040028;
        public static final int Gold=0x7f040012;
        public static final int Goldenrod=0x7f04003d;
        public static final int Gray=0x7f040061;
        public static final int Green=0x7f040089;
        public static final int GreenYellow=0x7f04004f;
        public static final int Honeydew=0x7f04002f;
        public static final int HotPink=0x7f040019;
        public static final int IndianRed=0x7f040044;
        public static final int Indigo=0x7f040072;
        public static final int Ivory=0x7f040002;
        public static final int Khaki=0x7f040031;
        public static final int KyleGrey=0x7f040023;
        public static final int KylePurple=0x7f040025;
        public static final int KyleTransGrey=0x7f040024;
        public static final int Lavender=0x7f040036;
        public static final int LavenderBlush=0x7f04000a;
        public static final int LawnGreen=0x7f040067;
        public static final int LemonChiffon=0x7f040007;
        public static final int LightBlue=0x7f040050;
        public static final int LightCoral=0x7f040032;
        public static final int LightCyan=0x7f040037;
        public static final int LightGoldenrodYellow=0x7f040021;
        public static final int LightGreen=0x7f040059;
        public static final int LightGrey=0x7f040040;
        public static final int LightPink=0x7f040014;
        public static final int LightSalmon=0x7f040016;
        public static final int LightSeaGreen=0x7f04007d;
        public static final int LightSkyBlue=0x7f04005f;
        public static final int LightSlateGray=0x7f040069;
        public static final int LightSteelBlue=0x7f04004d;
        public static final int LightYellow=0x7f040003;
        public static final int Lime=0x7f040083;
        public static final int LimeGreen=0x7f040079;
        public static final int Linen=0x7f040022;
        public static final int Magenta=0x7f04001e;
        public static final int Maroon=0x7f040064;
        public static final int MediumAquamarine=0x7f04006e;
        public static final int MediumBlue=0x7f04008c;
        public static final int MediumOrchid=0x7f040049;
        public static final int MediumPurple=0x7f040058;
        public static final int MediumSeaGreen=0x7f040078;
        public static final int MediumSlateBlue=0x7f040068;
        public static final int MediumSpringGreen=0x7f040084;
        public static final int MediumTurquoise=0x7f040073;
        public static final int MediumVioletRed=0x7f040045;
        public static final int MidnightBlue=0x7f04007f;
        public static final int MintCream=0x7f040029;
        public static final int MistyRose=0x7f04000d;
        public static final int Moccasin=0x7f04000f;
        public static final int NavajoWhite=0x7f040010;
        public static final int Navy=0x7f04008e;
        public static final int OldLace=0x7f040020;
        public static final int Olive=0x7f040062;
        public static final int OliveDrab=0x7f04006b;
        public static final int Orange=0x7f040015;
        public static final int OrangeRed=0x7f04001b;
        public static final int Orchid=0x7f04003e;
        public static final int PaleGoldenrod=0x7f040033;
        public static final int PaleGreen=0x7f040056;
        public static final int PaleTurquoise=0x7f04004e;
        public static final int PaleVioletRed=0x7f04003c;
        public static final int PapayaWhip=0x7f04000b;
        public static final int PeachPuff=0x7f040011;
        public static final int Peru=0x7f040043;
        public static final int Pink=0x7f040013;
        public static final int Plum=0x7f040039;
        public static final int PowderBlue=0x7f04004c;
        public static final int Purchace=0x7f040000;
        public static final int Purple=0x7f040063;
        public static final int Red=0x7f04001f;
        public static final int RosyBrown=0x7f040048;
        public static final int RoyalBlue=0x7f040076;
        public static final int SaddleBrown=0x7f04005b;
        public static final int Salmon=0x7f040027;
        public static final int SandyBrown=0x7f04002d;
        public static final int SeaGreen=0x7f04007b;
        public static final int Seashell=0x7f040009;
        public static final int Sienna=0x7f040053;
        public static final int Silver=0x7f040046;
        public static final int SkyBlue=0x7f040060;
        public static final int SlateBlue=0x7f04006c;
        public static final int SlateGray=0x7f04006a;
        public static final int Snow=0x7f040005;
        public static final int SpringGreen=0x7f040082;
        public static final int SteelBlue=0x7f040075;
        public static final int Tan=0x7f040041;
        public static final int Teal=0x7f040088;
        public static final int Thistle=0x7f04003f;
        public static final int Tomato=0x7f04001a;
        public static final int Turquoise=0x7f040077;
        public static final int Violet=0x7f040034;
        public static final int Wheat=0x7f04002c;
        public static final int White=0x7f040001;
        public static final int WhiteSmoke=0x7f04002a;
        public static final int Yellow=0x7f040004;
        public static final int YellowGreen=0x7f040054;
    }
    public static final class drawable {
        public static final int add_down=0x7f020000;
        public static final int add_item_button=0x7f020001;
        public static final int add_up=0x7f020002;
        public static final int amazon=0x7f020003;
        public static final int anandwani=0x7f020004;
        public static final int arbitrage_down=0x7f020005;
        public static final int arbitrage_up=0x7f020006;
        public static final int asus=0x7f020007;
        public static final int be_social_down=0x7f020008;
        public static final int be_social_up=0x7f020009;
        public static final int bottom_background_banner=0x7f02000a;
        public static final int btn_add=0x7f02000b;
        public static final int btn_arbitrage=0x7f02000c;
        public static final int btn_facebook=0x7f02000d;
        public static final int btn_home=0x7f02000e;
        public static final int btn_map=0x7f02000f;
        public static final int btn_seekers=0x7f020010;
        public static final int btn_submit=0x7f020011;
        public static final int contador_wine=0x7f020012;
        public static final int ebay=0x7f020013;
        public static final int facebook_login_down=0x7f020014;
        public static final int facebook_login_up=0x7f020015;
        public static final int falkner_wine=0x7f020016;
        public static final int gradient_bg=0x7f020017;
        public static final int gradient_bg_hover=0x7f020018;
        public static final int home_down=0x7f020019;
        public static final int home_up=0x7f02001a;
        public static final int ic_launcher=0x7f02001b;
        public static final int icon_96x96=0x7f02001c;
        public static final int image_bg=0x7f02001d;
        public static final int imperial_stout_464x464=0x7f02001e;
        public static final int item=0x7f02001f;
        public static final int jordan_olds=0x7f020020;
        public static final int jordan_one=0x7f020021;
        public static final int jordan_teams=0x7f020022;
        public static final int jordan_two=0x7f020023;
        public static final int jordan_vii=0x7f020024;
        public static final int kindle=0x7f020025;
        public static final int ksleeper=0x7f020026;
        public static final int kylecircle=0x7f020027;
        public static final int list_image_background=0x7f020028;
        public static final int list_row=0x7f020029;
        public static final int list_selector=0x7f02002a;
        public static final int map_down=0x7f02002b;
        public static final int map_up=0x7f02002c;
        public static final int molson_wine=0x7f02002d;
        public static final int nmeliones=0x7f02002e;
        public static final int pin=0x7f02002f;
        public static final int rasputin_ale=0x7f020030;
        public static final int rectangle=0x7f020031;
        public static final int retro_one=0x7f020032;
        public static final int rfavero=0x7f020033;
        public static final int rihanna=0x7f020034;
        public static final int seekers_down=0x7f020035;
        public static final int seekers_up=0x7f020036;
        public static final int sold_overlay=0x7f020037;
        public static final int splash=0x7f020038;
        public static final int submit_down=0x7f020039;
        public static final int submit_up=0x7f02003a;
        public static final int tab_arbitrage=0x7f02003b;
        public static final int tab_social=0x7f02003c;
        public static final int vineyard_wine=0x7f02003d;
    }
    public static final class id {
        public static final int addButton=0x7f08000f;
        public static final int addItem=0x7f08003e;
        public static final int addItemButton=0x7f080005;
        public static final int arbButton=0x7f08000d;
        public static final int artist=0x7f080051;
        public static final int auctionDuration=0x7f080006;
        public static final int backButton=0x7f080041;
        public static final int beSocial=0x7f08003f;
        public static final int bidTv=0x7f080032;
        public static final int container=0x7f080048;
        public static final int current_options=0x7f080021;
        public static final int description=0x7f080034;
        public static final int duration=0x7f080052;
        public static final int durationSeek=0x7f080007;
        public static final int execute=0x7f08001c;
        public static final int flare=0x7f080004;
        public static final int homButton=0x7f08003d;
        public static final int homeBtn1=0x7f080023;
        public static final int homeBtn2=0x7f080024;
        public static final int homeBtn3=0x7f080025;
        public static final int homeBtn4=0x7f080027;
        public static final int homeBtn5=0x7f080028;
        public static final int homeBtn6=0x7f080029;
        public static final int homeBtn7=0x7f08002b;
        public static final int homeBtn8=0x7f08002c;
        public static final int homeBtn9=0x7f08002d;
        public static final int homeButton=0x7f08000b;
        public static final int homeZoomImage=0x7f080033;
        public static final int icon=0x7f08003c;
        public static final int imageView1=0x7f08001a;
        public static final int imageView2=0x7f08001b;
        public static final int item=0x7f080013;
        public static final int item1=0x7f080047;
        public static final int itemDescription=0x7f080002;
        public static final int itemImage=0x7f080012;
        public static final int itemImage1=0x7f080044;
        public static final int itemPrice=0x7f080003;
        public static final int kyle=0x7f08002e;
        public static final int launcherIcon=0x7f080043;
        public static final int linLayout1=0x7f080022;
        public static final int linLayout2=0x7f080026;
        public static final int linLayout3=0x7f08002a;
        public static final int list=0x7f080010;
        public static final int listItemImage1=0x7f080039;
        public static final int list_image=0x7f08004f;
        public static final int list_view_left=0x7f08003a;
        public static final int list_view_right=0x7f08003b;
        public static final int listview_background_shape=0x7f080053;
        public static final int location=0x7f080038;
        public static final int mapButton=0x7f08000e;
        public static final int menuBar=0x7f08000a;
        public static final int menu_settings=0x7f080054;
        public static final int name=0x7f080017;
        public static final int names=0x7f080018;
        public static final int newItemImage=0x7f080001;
        public static final int pinImage=0x7f080037;
        public static final int price=0x7f080014;
        public static final int profit=0x7f080019;
        public static final int scanBarcode=0x7f080042;
        public static final int seekApproval=0x7f080040;
        public static final int seekerButton=0x7f08000c;
        public static final int storeImage=0x7f080015;
        public static final int storeImageLeft=0x7f080045;
        public static final int storeImageRight=0x7f080046;
        public static final int tab1=0x7f08004a;
        public static final int tab2=0x7f08004c;
        public static final int tabTitleText=0x7f08001f;
        public static final int tab_one=0x7f08004b;
        public static final int tabhost=0x7f080049;
        public static final int tabsLayout=0x7f08004d;
        public static final int tabsText=0x7f08004e;
        public static final int text=0x7f080020;
        public static final int textLeft=0x7f080008;
        public static final int textRight=0x7f080009;
        public static final int textView1=0x7f08001d;
        public static final int textView2=0x7f08001e;
        public static final int thumbnail=0x7f080011;
        public static final int timeLeft=0x7f080036;
        public static final int timeLeftText=0x7f080035;
        public static final int title=0x7f080050;
        public static final int tvBidPrice=0x7f080031;
        public static final int tvTime=0x7f080030;
        public static final int type=0x7f080016;
        public static final int useFacebookButton=0x7f080000;
        public static final int username=0x7f08002f;
    }
    public static final class layout {
        public static final int activity_login=0x7f030000;
        public static final int add_item=0x7f030001;
        public static final int child=0x7f030002;
        public static final int child_list_row=0x7f030003;
        public static final int current_arbitrage=0x7f030004;
        public static final int custom_tab=0x7f030005;
        public static final int facebook=0x7f030006;
        public static final int fragment_list=0x7f030007;
        public static final int gradient_bg=0x7f030008;
        public static final int gradient_bg_hover=0x7f030009;
        public static final int hello_world=0x7f03000a;
        public static final int history_arbitrage=0x7f03000b;
        public static final int home_screen=0x7f03000c;
        public static final int home_screen_act=0x7f03000d;
        public static final int home_screen_zoom=0x7f03000e;
        public static final int item=0x7f03000f;
        public static final int items_list=0x7f030010;
        public static final int list_item_icon_text=0x7f030011;
        public static final int main_menu=0x7f030012;
        public static final int main_menu2=0x7f030013;
        public static final int map=0x7f030014;
        public static final int options_list_row=0x7f030015;
        public static final int splash=0x7f030016;
        public static final int tab=0x7f030017;
        public static final int tab_layout=0x7f030018;
        public static final int tabs_bg=0x7f030019;
    }
    public static final class menu {
        public static final int activity_login=0x7f070000;
    }
    public static final class string {
        public static final int app_name=0x7f050000;
        public static final int arbitrage=0x7f05000b;
        public static final int bid=0x7f050006;
        public static final int curbid=0x7f050007;
        public static final int default_string=0x7f050002;
        public static final int description=0x7f050005;
        public static final int duration=0x7f050010;
        public static final int hello_world=0x7f050001;
        public static final int hr=0x7f050012;
        public static final int ksleeps=0x7f050008;
        public static final int location=0x7f05000d;
        public static final int menu_settings=0x7f050003;
        public static final int min=0x7f050011;
        public static final int powered=0x7f05000e;
        public static final int price=0x7f050004;
        public static final int profit=0x7f05000c;
        public static final int risk=0x7f05000f;
        public static final int stout_desc=0x7f050009;
        public static final int time_rem=0x7f05000a;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
