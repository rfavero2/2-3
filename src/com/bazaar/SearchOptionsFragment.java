package com.bazaar;

import java.util.ArrayList;
import java.util.List;

import com.utils.ParseUtils;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class SearchOptionsFragment extends Activity {
	/** Called when the activity is first created. */

	List<ShoppingDataOptions> listFull = new ArrayList<ShoppingDataOptions>();

	private ListView myList;
	private ShoppingDataOptionsAdapter adapter;
	ImageButton myBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.history_arbitrage);
		
        ImageButton ib;
        
        myBtn = (ImageButton) findViewById(R.id.arbButton);

        myBtn.setPressed(true);
        myBtn.setClickable(false);
        
        ib = (ImageButton) findViewById(R.id.seekerButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(SearchOptionsFragment.this, SeekerListActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.homeButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);accountIntent.putExtra("index", 1);
				accountIntent.setClass(SearchOptionsFragment.this, HomeScreenActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.mapButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(SearchOptionsFragment.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.addButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(SearchOptionsFragment.this, ItemsActivityValid.class);
				startActivity(accountIntent);					
        	}
        });
        
        
		

		listFull.add(new ShoppingDataOptions("Asus Nexus 7", "190", "265", "asus"));
		listFull.add(new ShoppingDataOptions("Air Jordan Retro I", "$150",
				"$225", "retro_one"));
		listFull.add(new ShoppingDataOptions("Kindle Fire HQ", "$170",
				"$269", "kindle"));

		adapter = new ShoppingDataOptionsAdapter(this,
				R.layout.options_list_row, listFull);

		// Set list adapter
		myList = (ListView) findViewById(R.id.current_options);
		myList.setAdapter(adapter);
	
	}

	
}