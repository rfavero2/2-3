package com.bazaar;

import java.util.ArrayList;
import java.util.List;

import com.utils.ParseUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class SearchDealActivity extends Activity {
	/** Called when the activity is first created. */

	List<ShoppingData> listEmpty = new ArrayList<ShoppingData>();
	List<ShoppingData> listFull = new ArrayList<ShoppingData>();

	private ListView myList;
	private EditText nameCapture;
	private ShoppingDataAdapter adapter;
	private TextView profit;
	private Button arbitrage;

	private ImageButton myBtn;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.current_arbitrage);

		
        ImageButton ib;
        
        myBtn = (ImageButton) findViewById(R.id.arbButton);

        myBtn.setPressed(true);
        myBtn.setClickable(false);
        
        ib = (ImageButton) findViewById(R.id.seekerButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(SearchDealActivity.this, SeekerListActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.homeButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);accountIntent.putExtra("index", 1);
				accountIntent.setClass(SearchDealActivity.this, HomeScreenActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.mapButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(SearchDealActivity.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.addButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(SearchDealActivity.this, ItemsActivityValid.class);
				startActivity(accountIntent);					
        	}
        });
        
        
		
		arbitrage = (Button) findViewById(R.id.execute);
		profit = (TextView) findViewById(R.id.profit);

		listFull.add(new ShoppingData("Air Jordan Retro I", "$150", "Buy Now",
				"ebay", "jordan_one"));
		listFull.add(new ShoppingData("Air Jordan Retro I", "$225",
				"10 Sold at $225", "amazon", "jordan_two"));

		nameCapture = (EditText) findViewById(R.id.name);
		adapter = new ShoppingDataAdapter(ParseUtils.get23Context(),
				R.layout.child_list_row, listEmpty);

		// Set list adapter
		myList = (ListView) findViewById(R.id.names);
		myList.setAdapter(adapter);

		AlterAdapter();


		arbitrage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent accountIntent = new Intent();
				accountIntent.setClass(SearchDealActivity.this, SearchOptionsFragment.class);
				startActivity(accountIntent);
			}
		});
		
		nameCapture.addTextChangedListener(new TextWatcher() {

			// As the user types in the search field, the list is
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				AlterAdapter();
			}

			// Not used for this program
			@Override
			public void afterTextChanged(Editable arg0) {

			}

			// Not uses for this program
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}
		});

		
		
	}
/*
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.current_arbitrage, container, false);

		arbitrage = (Button) v.findViewById(R.id.execute);
		profit = (TextView) v.findViewById(R.id.profit);

		listFull.add(new ShoppingData("Air Jordan Retro I", "$150", "Buy Now",
				"ebay", "jordan_one"));
		listFull.add(new ShoppingData("Air Jordan Retro I", "$225",
				"10 Sold at $225", "amazon", "jordan_two"));

		nameCapture = (EditText) v.findViewById(R.id.name);
		adapter = new ShoppingDataAdapter(ParseUtils.get23Context(),
				R.layout.child_list_row, listEmpty);

		// Set list adapter
		myList = (ListView) v.findViewById(R.id.names);
		myList.setAdapter(adapter);

		AlterAdapter();


		arbitrage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.setClass(getActivity(), SearchOptionsFragment.class);
				startActivity(accountIntent);
			}
		});
		
		nameCapture.addTextChangedListener(new TextWatcher() {

			// As the user types in the search field, the list is
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				AlterAdapter();
			}

			// Not used for this program
			@Override
			public void afterTextChanged(Editable arg0) {

			}

			// Not uses for this program
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}
		});

		return v;
	}
*/
	// Filters list of contacts based on user search criteria. If no information
	// is filled in, contact list will be blank.
	private void AlterAdapter() {
		Log.w("com.bazaar", "Alert Adapter!");
		if (nameCapture.getText().toString().isEmpty()) {
			listEmpty.clear();
			adapter.notifyDataSetChanged();
			profit.setVisibility(View.GONE);
			arbitrage.setVisibility(View.GONE);
		} else {
			listEmpty.clear();
			for (int i = 0; i < 2; i++) {
				if (nameCapture.getText().toString().length() > 4) {
					if (listFull.get(i).getStoreImage().toString()
							.contains(nameCapture.getText().toString())) {
						listEmpty.add(listFull.get(i));
						profit.setVisibility(View.VISIBLE);
						arbitrage.setVisibility(View.VISIBLE);
					}
				}
				adapter.notifyDataSetChanged();
			}
		}
	}
}