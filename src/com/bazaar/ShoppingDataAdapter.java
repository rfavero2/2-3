package com.bazaar;

import java.util.List;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ShoppingDataAdapter extends ArrayAdapter<ShoppingData> {

	private int				resource;
	private LayoutInflater	inflater;
	private Context 		context;
	
	public ShoppingDataAdapter ( Context ctx, int resourceId, List<ShoppingData> objects) {	
		super( ctx, resourceId, objects );
		resource = resourceId;
		inflater = LayoutInflater.from( ctx );
		context=ctx;
	}


	@Override
	public View getView ( int position, View convertView, ViewGroup parent ) {
	
		convertView = ( RelativeLayout ) inflater.inflate( resource, null );

		ShoppingData sData = getItem( position );
				
		TextView item = (TextView) convertView.findViewById(R.id.item);
		item.setText(sData.getItem());
		
		TextView price = (TextView) convertView.findViewById(R.id.price);
		price.setText(sData.getPrice());
		
		TextView type = (TextView) convertView.findViewById(R.id.type);
		type.setText(sData.getType());
		
		
		ImageView itemImage = (ImageView) convertView.findViewById(R.id.storeImage);
		String uri = "drawable/" + sData.getItemImage();
	    int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
	    Drawable image = context.getResources().getDrawable(imageResource);
	    itemImage.setImageDrawable(image);
	    
		ImageView storeImage = (ImageView) convertView.findViewById(R.id.itemImage);
		String uri1 = "drawable/" + sData.getStoreImage();
	    int imageResource1 = context.getResources().getIdentifier(uri1, null, context.getPackageName());
	    Drawable image1 = context.getResources().getDrawable(imageResource1);
	    storeImage.setImageDrawable(image1);

		return convertView;

	}
}


