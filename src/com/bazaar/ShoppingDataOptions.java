package com.bazaar;



public class ShoppingDataOptions {
	
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getPrice1() {
		return price1;
	}
	public void setPrice1(String price1) {
		this.price1 = price1;
	}
	public String getPrice2() {
		return price2;
	}
	public void setPrice2(String price2) {
		this.price2 = price2;
	}
	public String getItemImage() {
		return itemImage;
	}
	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}
	public ShoppingDataOptions(String item, String price1, String price2,
			String itemImage) {
		super();
		this.item = item;
		this.price1 = price1;
		this.price2 = price2;
		this.itemImage = itemImage;
	}
	String item;
	String price1;
	String price2;
	String itemImage;


}
