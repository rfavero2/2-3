package com.bazaar;


import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class HomeZoomActivity extends Activity {
	TextView mTimeLeftText;
	static int mTimeLeft = 300;
	static final int timeDelay = 1000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_screen_zoom);
		
		TextView tv = (TextView) findViewById(R.id.bidTv);
		tv.bringToFront();
		
		Typeface type = Typeface.createFromAsset(getAssets(),"fonts/pirulen.ttf"); 
		tv.setTypeface(type);
		
		TextView time = (TextView) findViewById(R.id.tvBidPrice);
        time.setText("$92.00");
		time.setTypeface(type);
		
		TextView username =  (TextView) findViewById(R.id.username);
		username.setTypeface(type);
		
		TextView curbid = (TextView) findViewById(R.id.tvTime);
		curbid.setTypeface(type);
		
		TextView desc = (TextView) findViewById(R.id.description);
		desc.setTypeface(type);		
		
		TextView timeLeft = (TextView) findViewById(R.id.timeLeft);
		timeLeft.setTypeface(type);
		timeLeft.bringToFront();
		
		mTimeLeftText = (TextView) findViewById(R.id.timeLeftText);
		mTimeLeftText.setTypeface(type);
		setTime();
		mTimeLeftText.bringToFront();
		
		final Handler h= new Handler();
		h.post(new Runnable(){

		        @Override
		        public void run() {
		            // call your function
		            h.postDelayed(this,timeDelay);
		            if(0 != mTimeLeft)
		            	mTimeLeft--;
		            setTime();
		        }

		    });

	}
	
	private void setTime() {
		String result = String.format("%02d:%02d", mTimeLeft / 60, mTimeLeft % 60);
		TextView tv = (TextView) findViewById(R.id.timeLeft);

		tv.setText(result);
	}
}
