package com.bazaar;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ItemsAdapter extends ArrayAdapter<ItemData> {

	Context context;
	LayoutInflater inflater;
	int layoutResourceId;
	float imageWidth;
	private final ItemData[] data;

	public ItemsAdapter(Context context, int layoutResourceId, ItemData[] items) {
		super(context, layoutResourceId, items);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.data = items;

		DisplayMetrics metrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(metrics);

		float width = metrics.widthPixels;
		float margin = (int) convertDpToPixel(10f, (Activity) context);

		imageWidth = ((width - (3 * margin)) / 2);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout row = (RelativeLayout) convertView;
		ItemHolder holder;
		ItemData item = data[position];

		if (row == null) {
			holder = new ItemHolder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = (RelativeLayout) inflater.inflate(layoutResourceId, parent,
					false);
			ImageView itemImage = (ImageView) row.findViewById(R.id.listItemImage1);
			holder.itemImage = itemImage;
			TextView price  = (TextView) row.findViewById(R.id.textView1);
			price.setText(Float.toString(item.getItemPrice()));
			TextView description  = (TextView) row.findViewById(R.id.textView2);
			description.setText(item.getItemDescription()); 
		} else {
			holder = (ItemHolder) row.getTag();
		}

		row.setTag(holder);
		setImageBitmap(item.getItemPhoto(), holder.itemImage);
		return row;
	}

	public static class ItemHolder {
		ImageView itemImage;
	}

	// resize the image proportionately so it fits the entire space
	private void setImageBitmap(Bitmap item, ImageView imageView) {
		Bitmap bitmap = item;
		if (bitmap != null) {
			float i = ((float) imageWidth) / ((float) bitmap.getWidth());
			float imageHeight = i * (bitmap.getHeight());
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView
					.getLayoutParams();
			params.height = (int) imageHeight;
			params.width = (int) imageWidth;
			imageView.setLayoutParams(params);
			imageView.setImageBitmap(item);
		}
	}

	public static float convertDpToPixel(float dp, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

}