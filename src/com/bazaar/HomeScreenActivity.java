package com.bazaar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;


public class HomeScreenActivity extends Activity {

	ImageButton myBtn;
    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen_act);
        //mNum = getArguments() != null ? getArguments().getInt("num") : 1;
        ImageButton ib;
        
        myBtn = (ImageButton) findViewById(R.id.homeButton);

        myBtn.setPressed(true);
        myBtn.setClickable(false);
        
        ib = (ImageButton) findViewById(R.id.seekerButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(HomeScreenActivity.this, SeekerListActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.arbButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 1);
				accountIntent.setClass(HomeScreenActivity.this, SearchDealActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.mapButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 1);
				accountIntent.setClass(HomeScreenActivity.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.addButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(HomeScreenActivity.this, ItemsActivityValid.class);
				startActivity(accountIntent);					
        	}
        });
        
        
        
        
        
        
        ib = (ImageButton) findViewById(R.id.homeBtn1);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 1);
				accountIntent.setClass(HomeScreenActivity.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });

        ib = (ImageButton) findViewById(R.id.homeBtn2);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 2);
				accountIntent.setClass(HomeScreenActivity.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.homeBtn3);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 3);
				accountIntent.setClass(HomeScreenActivity.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.homeBtn4);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 4);
				accountIntent.setClass(HomeScreenActivity.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.homeBtn5);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 5);
				accountIntent.setClass(HomeScreenActivity.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
  
        ib = (ImageButton) findViewById(R.id.homeBtn6);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 6);
				accountIntent.setClass(HomeScreenActivity.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.homeBtn7);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 7);
				accountIntent.setClass(HomeScreenActivity.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
    }

}



