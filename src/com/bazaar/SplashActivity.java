package com.bazaar;

import com.parse.ParseUser;
import com.utils.ParseUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

public class SplashActivity extends Activity {
	protected int _splashTime = 3000;

	private Thread splashTread;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);
		
		ParseUtils.setContext(getApplicationContext());
	
		// TODO: Consider moving to splash screen busy time
		initializeApplication();
		// thread for displaying the SplashScreen
		splashTread = new Thread() {
			@Override
			public void run() {
				try {
					synchronized (this) {
						wait(_splashTime);
					}
				} catch (InterruptedException e) {
				} finally {
					
					// Check for the shared preference. If the shared preference
					// is set go to the appropriate parent or child page otherwise 
					// go to the walk-through
					Intent startIntent = new Intent();
					startIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					
					ParseUser currentUser = ParseUser.getCurrentUser();
					if (currentUser != null) {
					    // Logged in, bypass log-in screen
						// TODO: Switch directly to settings
						startIntent.setClass(SplashActivity.this, HomeScreenActivity.class);
					} else {
						// show the signup or login screen
						startIntent.setClass(SplashActivity.this, HomeScreenActivity.class);
					}
					
					startActivity(startIntent);
					finish();
				}
			}
		};
		splashTread.start();
		
	}
	
	private void initializeApplication() {
		// Initialize Parse
		ParseUtils.initialize(getApplicationContext());// , getString(R.string.app_id));
	}
}
