package com.bazaar;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;

import android.view.View;

import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageButton;
import android.widget.ListView;


public class ItemsActivityValid extends Activity {
	
	private ListView listViewLeft;
	private ListView listViewRight;
	private ItemsAdapter leftAdapter;
	private ItemsAdapter rightAdapter;
	private static final int CAMERA_REQUEST = 9090;
	public static ArrayList<ItemData> leftItems=new ArrayList<ItemData>();
	public static ArrayList<ItemData> rightItems=new ArrayList<ItemData>();
	private int[] leftViewsHeights;
	private int[] rightViewsHeights;
	
	ImageButton myBtn;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.items_list);
        
		ImageButton ib;
        
        myBtn = (ImageButton) findViewById(R.id.addButton);

        myBtn.setPressed(true);
        myBtn.setClickable(false);
        
        ib = (ImageButton) findViewById(R.id.homeButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(ItemsActivityValid.this, HomeScreenActivity.class);
				startActivity(accountIntent);					
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.seekerButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(ItemsActivityValid.this, SeekerListActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.arbButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 1);
				accountIntent.setClass(ItemsActivityValid.this, SearchDealActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.mapButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 1);
				accountIntent.setClass(ItemsActivityValid.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        

        
		
		
		
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
        startActivityForResult(cameraIntent, CAMERA_REQUEST); 
        
        
        
	}
                                               
	@Override
	public void onResume() {
		super.onResume();
		Log.w("com.bazaar", "onResume!");
	       // The reload fragment code here !
		/*
        if (this.isDetached()) {
            getFragmentManager().beginTransaction()
               .detach(this)
               .attach(this)
               .commit();
        }*/
	}
	
    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
	/*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {		    	    	
    	
        View v = inflater.inflate(R.layout.items_list, container, false);
		
		listViewLeft = (ListView) v.findViewById(R.id.list_view_left);
		listViewRight = (ListView) v.findViewById(R.id.list_view_right);
		
		loadItems();
		
		listViewLeft.setOnTouchListener(touchListener);
		listViewRight.setOnTouchListener(touchListener);		
		listViewLeft.setOnScrollListener(scrollListener);
		listViewRight.setOnScrollListener(scrollListener);
		
		ImageButton photoButton = (ImageButton) v.findViewById(R.id.addItem);
        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
            }
        });
        
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
        startActivityForResult(cameraIntent, CAMERA_REQUEST); 

        //tv.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.gallery_thumb));
        return v;
    }
	
	// Passing the touch event to the opposite list
	OnTouchListener touchListener = new OnTouchListener() {					
		boolean dispatched = false;
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (v.equals(listViewLeft) && !dispatched) {
				dispatched = true;
				listViewRight.dispatchTouchEvent(event);
			} else if (v.equals(listViewRight) && !dispatched) {
				dispatched = true;
				listViewLeft.dispatchTouchEvent(event);
			}
			
			dispatched = false;
			return false;
		}
	};
	*/
	/**
	 * Synchronizing scrolling 
	 * Distance from the top of the first visible element opposite list:
	 * sum_heights(opposite invisible screens) - sum_heights(invisible screens) + distance from top of the first visible child
	 */
	OnScrollListener scrollListener = new OnScrollListener() {
		
		@Override
		public void onScrollStateChanged(AbsListView v, int scrollState) {	
		}
		
		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			
			if (view.getChildAt(0) != null) {
				if (view.equals(listViewLeft) ){
					leftViewsHeights[view.getFirstVisiblePosition()] = view.getChildAt(0).getHeight();
					
					int h = 0;
					for (int i = 0; i < listViewRight.getFirstVisiblePosition(); i++) {
						h += rightViewsHeights[i];
					}
					
					int hi = 0;
					for (int i = 0; i < listViewLeft.getFirstVisiblePosition(); i++) {
						hi += leftViewsHeights[i];
					}
					
					int top = h - hi + view.getChildAt(0).getTop();
					listViewRight.setSelectionFromTop(listViewRight.getFirstVisiblePosition(), top);
				} else if (view.equals(listViewRight)) {
					rightViewsHeights[view.getFirstVisiblePosition()] = view.getChildAt(0).getHeight();
					
					int h = 0;
					for (int i = 0; i < listViewLeft.getFirstVisiblePosition(); i++) {
						h += leftViewsHeights[i];
					}
					
					int hi = 0;
					for (int i = 0; i < listViewRight.getFirstVisiblePosition(); i++) {
						hi += rightViewsHeights[i];
					}
					
					int top = h - hi + view.getChildAt(0).getTop();
					listViewLeft.setSelectionFromTop(listViewLeft.getFirstVisiblePosition(), top);
				}
				
			}
			
		}
	};
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {  
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {  
        	Log.w("com.bazaar", "got here!");
            Bitmap itemPhoto = (Bitmap) data.getExtras().get("data"); 
            Intent addItemIntent = new Intent(this, AddItemActivity.class);
            addItemIntent.putExtra("BitmapImage", itemPhoto);
            startActivity(addItemIntent);
        }  
    } 

	private void loadItems(){
		if (leftItems.size() > 0) {
			leftAdapter = new ItemsAdapter(this, R.layout.item,
					leftItems.toArray(new ItemData[leftItems.size()]));
			listViewLeft.setAdapter(leftAdapter);
			leftViewsHeights = new int[leftItems.size()];
		}

		if (rightItems.size() > 0) {
			rightAdapter = new ItemsAdapter(this, R.layout.item,
					rightItems.toArray(new ItemData[rightItems.size()]));
			listViewRight.setAdapter(rightAdapter);
			rightViewsHeights = new int[rightItems.size()];
		}
	}
	
	//METHOD WHICH WILL HANDLE DYNAMIC INSERTION
    public void addItems(View v) {
    	loadItems();
    	leftAdapter.notifyDataSetChanged();
    	rightAdapter.notifyDataSetChanged();
    }
}