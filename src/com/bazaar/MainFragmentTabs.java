package com.bazaar;


import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ActionBar.Tab;
import android.content.Intent;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import android.view.View;

import android.widget.ImageButton;

import android.widget.TabHost;




/**
 * This demonstrates the use of action bar tabs and how they interact
 * with other action bar features.
 */
public class MainFragmentTabs extends Activity {
	TabHost mTabHost;
	FragmentTransaction mFt;
	Activity me;
    private Fragment mFragment;

	private static final int CAMERA_REQUEST = 9090;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.main_menu);
        
/*
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        
        setContentView(R.layout.tab);
		TabHost th=(TabHost) findViewById(R.id.tabhost);
		th.setup();
		//ImageView iv = findViewById(R.layout.tab_one);
		TabSpec spec=th.newTabSpec("tag1");
		spec.setContent(R.id.tab1);
		spec.setIndicator("", getResources().getDrawable(R.drawable.tab_arbitrage));
		th.addTab(spec);
	    th.getTabWidget().getChildAt(0).setLayoutParams(new
                LinearLayout.LayoutParams((width/2)-2,50));
	    
	    spec=th.newTabSpec("tag2");
	    spec.setContent(R.id.tab2);
	    spec.setIndicator("", null);
	    th.addTab(spec);
	    th.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tab_arbitrage);
	    th.getTabWidget().getChildAt(1).setLayoutParams(new
                LinearLayout.LayoutParams((width/2)-2,50));
	   
	    th.addTab(th.newTabSpec("tab_test1")
	               .setIndicator((""),getResources().getDrawable(R.drawable.arbitrage_down))
	               .setContent(new Intent(this, AddItemActivity.class)));

        


	    th.getTabWidget().getChildAt(2).setLayoutParams(new
                LinearLayout.LayoutParams((width/2)-2,50));
	    
        int density = 1;
        int tabMinWidth = (int) (83 * density);
        int tabWidth = tabMinWidth;
       // int nrOfTabs = displayMetrics.widthPixels / tabMinWidth;
       // if (nrOfTabs > 4)
        //    tabWidth = displayMetrics.widthPixels / 5;
        for(int i = 0; i < 3; i++)
            th.getTabWidget().getChildAt(i).getLayoutParams().width = tabWidth;
        
  */      
	    
	    //th.addTab(tabHost.newTabSpec("aaa").setIndicator("", getResources().getDrawable(R.drawable.my_tabselector)).setContent(R.id.my_tab_contentlayout));
        // /*
        /*
        final ActionBar bar = getActionBar();
        
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
       
        //ActionBar.Tab tb = bar.newTab();
        
        Log.w("com.bazaar", "chkp1");
       // tb.setIcon(this.getResources().getDrawable(R.drawable.be_social_down));
        bar.addTab(bar.newTab()
                .setText("Home")
                //.setIcon(this.getResources().getDrawable(R.drawable.tab_social))
                //.setCustomView(findViewById(R.layout.custom_tab))
                .setTabListener(new TabListener<HomeScreenFragment>(
                        this, "simple", HomeScreenFragment.class)));
       // bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_CUSTOM);

        bar.addTab(bar.newTab()
                .setText("Seekers")
                //.setIcon(this.getResources().getDrawable(R.drawable.tab_arbitrage))
                .setTabListener(new TabListener<BountyHunterListFragment>(
                        this, "apps", BountyHunterListFragment.class))); 

        bar.addTab(bar.newTab()
                .setText("Arbitrage")
                .setTabListener(new TabListener<SearchDealFragment>(
                        this, "simple", SearchDealFragment.class)));
        bar.addTab(bar.newTab()
                .setText("Map")
                .setTabListener(new TabListener<BiddingWarsFragment>(
                        this, "simple", BiddingWarsFragment.class)));
        bar.addTab(bar.newTab()
                .setText("Add Item")
                .setTabListener(new TabListener<ItemsActivity>(
                        this, "simple", ItemsActivity.class)));
                                
        if (savedInstanceState != null) {
            bar.setSelectedNavigationItem(savedInstanceState.getInt("tab", 0));
        }
        
        /*
        setContentView(R.layout.tab_layout);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

       TabHost mTabHost = getTabHost();

       mTabHost.addTab(mTabHost.newTabSpec("tab_test1")
               .setIndicator((""),getResources().getDrawable(R.drawable.arbitrage_down))
         .setContent(new Intent(this, ItemsActivity.class)));
       mTabHost.addTab(mTabHost.newTabSpec("tab_test2")
               .setIndicator((""),getResources().getDrawable(R.drawable.arbitrage_down))
         .setContent(new Intent(this, ItemsActivity.class)));
               mTabHost.setCurrentTab(0);
               mTabHost.getTabWidget().getChildAt(0).setLayoutParams(new
                 LinearLayout.LayoutParams((width/2)-2,50));
          mTabHost.getTabWidget().getChildAt(1).setLayoutParams(new
                     LinearLayout.LayoutParams((width/2)-2,50));*/
        
        setContentView(R.layout.main_menu2);
		ImageButton arbButton = (ImageButton)findViewById(R.id.arbButton);
		
	//	mFt = 
		
		arbButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentTransaction ft = me.getFragmentManager().beginTransaction();
				
				
	            if (mFragment == null) {
	                mFragment = Fragment.instantiate(me.getApplicationContext(), HomeScreenFragment.class.getName(), null);
	                ft.add(android.R.id.content, mFragment, "woo");
	            } else {
	                ft.attach(mFragment);
	            }
			}
		});
		
    }

  
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("tab", getActionBar().getSelectedNavigationIndex());
    }

	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {  
            Bitmap itemPhoto = (Bitmap) data.getExtras().get("data"); 
            Intent addItemIntent = new Intent(this, AddItemActivity.class);
            addItemIntent.putExtra("BitmapImage", itemPhoto);
            startActivity(addItemIntent);
        }  
        Log.w("com.bazaar", "onActivityResult: " + requestCode + " " + resultCode);
    } 
    
    public static class TabListener<T extends Fragment> implements ActionBar.TabListener {
        private final Activity mActivity;
        private final String mTag;
        private final Class<T> mClass;
        private final Bundle mArgs;
        private Fragment mFragment;

        public TabListener(Activity activity, String tag, Class<T> clz) {
            this(activity, tag, clz, null);
        }

        public TabListener(Activity activity, String tag, Class<T> clz, Bundle args) {
            mActivity = activity;
            mTag = tag;
            mClass = clz;
            mArgs = args;
            // Check to see if we already have a fragment for this tab, probably
            // from a previously saved state.  If so, deactivate it, because our
            // initial state is that a tab isn't shown.
            mFragment = mActivity.getFragmentManager().findFragmentByTag(mTag);
            if (mFragment != null && !mFragment.isDetached()) {
                FragmentTransaction ft = mActivity.getFragmentManager().beginTransaction();
                ft.detach(mFragment);
                ft.commit();
            }
        }

        public void onTabSelected(Tab tab, FragmentTransaction ft) {
            if (mFragment == null) {
                mFragment = Fragment.instantiate(mActivity, mClass.getName(), mArgs);
                ft.add(android.R.id.content, mFragment, mTag);
            } else {
                ft.attach(mFragment);
            }
        }

        public void onTabUnselected(Tab tab, FragmentTransaction ft) {
            if (mFragment != null) {
                ft.detach(mFragment);
            }
        }

        public void onTabReselected(Tab tab, FragmentTransaction ft) {
        }
    }
    
}
