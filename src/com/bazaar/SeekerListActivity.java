/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bazaar;



import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class SeekerListActivity extends Activity
{

	// This is the Adapter being used to display the list's data.
	SeekerListAdapter mAdapter;
	ListView mList;
	// If non-null, this is the current filter the user has provided.
	String mCurFilter;
	ImageButton myBtn;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) { 	
        super.onCreate(savedInstanceState);   
        setContentView(R.layout.child);
        
        ImageButton ib;
        
        myBtn = (ImageButton) findViewById(R.id.seekerButton);

        myBtn.setPressed(true);
        myBtn.setClickable(false);
        
        ib = (ImageButton) findViewById(R.id.homeButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.setClass(SeekerListActivity.this, HomeScreenActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.arbButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 1);
				accountIntent.setClass(SeekerListActivity.this, SearchDealActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.mapButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 1);
				accountIntent.setClass(SeekerListActivity.this, HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) findViewById(R.id.addButton);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(SeekerListActivity.this, ItemsActivityValid.class);
				startActivity(accountIntent);				
        	}
        });
        
        mList = (ListView)findViewById(R.id.list);
        
		List<Seeker> seekerList = new ArrayList<Seeker>();
		seekerList.add(new Seeker("Nic Meliones", this.getResources().getDrawable(R.drawable.nmeliones))
				 );
		seekerList.add(new Seeker("Rihanna", this.getResources().getDrawable(R.drawable.rihanna))
				 );
		seekerList.add(new Seeker("Kyle Sleeper", this.getResources().getDrawable(R.drawable.ksleeper))
				 );
		seekerList.add(new Seeker("Ankur Nandwani", this.getResources().getDrawable(R.drawable.anandwani))
				 );
		seekerList.add(new Seeker("Ross Favero", this.getResources().getDrawable(R.drawable.rfavero))
				 );
		
		// Create an empty adapter we will use to display the loaded data.
		mAdapter = new SeekerListAdapter(this);
        mAdapter.setData(seekerList);
        
        mList.setAdapter(mAdapter);
    }

    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
	/*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        //View v = inflater.inflate(R.layout.fragment_list, container, false);

        //return v;
        View view = inflater.inflate(R.layout.fragment_list, null);
        return view;
    }
    */
    /*
	@Override public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	    
		//setContentView(R.layout.items_list);	
		
		// Give some text to display if there is no data.  In a real
		// application this would come from a resource.
		setEmptyText("No applications");
		
		// We have a menu item to show in action bar.
		setHasOptionsMenu(true);
		
		// Create an empty adapter we will use to display the loaded data.
		mAdapter = new BountyHunterListAdapter(getActivity());
		setListAdapter(mAdapter);
		
		// Start out with a progress indicator.
		setListShown(false);

		List<BountyHunter> bountyHunterList = new ArrayList<BountyHunter>();
		bountyHunterList.add(new BountyHunter("Nic Meliones", getActivity().getResources().getDrawable(R.drawable.nmeliones))
				 );
		bountyHunterList.add(new BountyHunter("Rihanna", getActivity().getResources().getDrawable(R.drawable.rihanna))
				 );
		bountyHunterList.add(new BountyHunter("Kyle Sleeper", getActivity().getResources().getDrawable(R.drawable.ksleeper))
				 );
		bountyHunterList.add(new BountyHunter("Ankur Nandwani", getActivity().getResources().getDrawable(R.drawable.anandwani))
				 );
		bountyHunterList.add(new BountyHunter("Ross Favero", getActivity().getResources().getDrawable(R.drawable.rfavero))
				 );
		
		mAdapter.setData(bountyHunterList);
		
		setListShown(true);
	}
*/
    /*
	@Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Place an action bar item for searching.
		MenuItem item = menu.add("Search");
		item.setIcon(android.R.drawable.ic_menu_search);
		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
		       | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		SearchView sv = new SearchView(getActivity());
		//sv.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.bottom_background_banner));
		item.setActionView(sv);
	}

	@Override public void onListItemClick(ListView l, View v, int position, long id) {
	// Insert desired behavior here.
	Log.i("LoaderCustom", "Item clicked: " + id);
	}
	*/
}

class Seeker {
	private String mName;
	private Drawable mIcon;
	
	Seeker(String name, Drawable icon) {
		mName = name;
		mIcon = icon;
	}
	
	public Drawable getIcon() {
		return mIcon;
	}
	
	public String getName() {
		return mName;
	}
}

class SeekerListAdapter extends ArrayAdapter<Seeker> {
    private final LayoutInflater mInflater;

    public SeekerListAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_2);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(List<Seeker> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }

    /**
     * Populate new items in the list.
     */
    @Override public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_icon_text, parent, false);
        } else {
            view = convertView;
        }

        Seeker item = getItem(position);
        ((ImageView)view.findViewById(R.id.icon)).setImageDrawable(item.getIcon());
        ((TextView)view.findViewById(R.id.text)).setText(item.getName());
        
        return view;
    }
}


