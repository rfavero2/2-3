package com.bazaar;

import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ShoppingDataOptionsAdapter extends ArrayAdapter<ShoppingDataOptions>{
	
	private int				resource;
	private LayoutInflater	inflater;
	private Context 		context;
	
	public ShoppingDataOptionsAdapter ( Context ctx, int resourceId, List<ShoppingDataOptions> objects) {	
		super( ctx, resourceId, objects );
		resource = resourceId;
		inflater = LayoutInflater.from( ctx );
		context=ctx;
	}


	@Override
	public View getView ( int position, View convertView, ViewGroup parent ) {
	
		convertView = ( RelativeLayout ) inflater.inflate( resource, null );

		ShoppingDataOptions sData = getItem( position );
				
		TextView item = (TextView) convertView.findViewById(R.id.item1);
		item.setText(sData.getItem());
		
		TextView price1 = (TextView) convertView.findViewById(R.id.textLeft);
		price1.setText(sData.getPrice1());
		
		TextView price2 = (TextView) convertView.findViewById(R.id.textRight);
		price2.setText(sData.getPrice2());
				
		ImageView itemImage = (ImageView) convertView.findViewById(R.id.itemImage1);
		String uri = "drawable/" + sData.getItemImage();
	    int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
	    Drawable image = context.getResources().getDrawable(imageResource);
	    itemImage.setImageDrawable(image);
	    
	    ImageView itemImage1 = (ImageView) convertView.findViewById(R.id.imageView1);
	    if(position == 1) {
	    	itemImage1.setVisibility(View.VISIBLE);
	    }
		return convertView;

	}

}
