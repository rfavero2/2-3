package com.bazaar;

import android.graphics.drawable.Drawable;

public class AuctionObject {
	String mDescription;
	double mPrice;
	double mCurrentBid;
	Drawable mIcon;
	
	AuctionObject(String description, double price, double currentBid, Drawable icon) {
		mDescription = description;
		mPrice = price;
		mCurrentBid = currentBid;
		mIcon = icon;
	}
}
