package com.bazaar;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;


public class HomeScreenFragment extends Fragment {

    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //mNum = getArguments() != null ? getArguments().getInt("num") : 1;
    }

    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_screen, container, false);
        ImageButton ib;
        
        
        ib = (ImageButton) v.findViewById(R.id.homeBtn1);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 1);
				accountIntent.setClass(getActivity(), HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });

        ib = (ImageButton) v.findViewById(R.id.homeBtn2);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 2);
				accountIntent.setClass(getActivity(), HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) v.findViewById(R.id.homeBtn3);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 3);
				accountIntent.setClass(getActivity(), HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) v.findViewById(R.id.homeBtn4);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 4);
				accountIntent.setClass(getActivity(), HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) v.findViewById(R.id.homeBtn5);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 5);
				accountIntent.setClass(getActivity(), HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
  
        ib = (ImageButton) v.findViewById(R.id.homeBtn6);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 6);
				accountIntent.setClass(getActivity(), HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        
        ib = (ImageButton) v.findViewById(R.id.homeBtn7);
        ib.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// Dirrrrty
				Intent accountIntent = new Intent();
				accountIntent.putExtra("index", 7);
				accountIntent.setClass(getActivity(), HomeZoomActivity.class);
				startActivity(accountIntent);			
        	}
        });
        return v;
    }
}



