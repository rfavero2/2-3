package com.bazaar;

public class ShoppingData {
	
	public ShoppingData(String _item, String _price, String _type, String _itemImage, String _storeImage) {
		item = _item;
		price = _price;
		type = _type;
		itemImage = _itemImage;
		storeImage = _storeImage;
		
	}
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getItemImage() {
		return itemImage;
	}
	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}
	public String getStoreImage() {
		return storeImage;
	}
	public void setStoreImage(String storeImage) {
		this.storeImage = storeImage;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	String item;
	String price;
	String itemImage;
	String storeImage;
	String type;

}
