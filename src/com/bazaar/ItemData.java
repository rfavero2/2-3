/*
 * This class provides an interface to all the data 
 * of a particular item
 */
package com.bazaar;

import android.graphics.Bitmap;

public class ItemData {
	
	private Bitmap itemPhoto;
	private float itemPrice;
	private String itemDescription;
	
	public ItemData(Bitmap _itemPhoto, float _itemPrice, String _itemDesscription) {
		this.setItemPhoto(_itemPhoto);
		this.setItemPrice(_itemPrice);
		this.setItemDescription(_itemDesscription); 
	}

	/**
	 * @return the itemPhoto
	 */
	public Bitmap getItemPhoto() {
		return itemPhoto;
	}

	/**
	 * @param itemPhoto the itemPhoto to set
	 */
	public void setItemPhoto(Bitmap itemPhoto) {
		this.itemPhoto = itemPhoto;
	}

	/**
	 * @return the itemPrice
	 */
	public float getItemPrice() {
		return itemPrice;
	}

	/**
	 * @param itemPrice the itemPrice to set
	 */
	public void setItemPrice(float itemPrice) {
		this.itemPrice = itemPrice;
	}

	/**
	 * @return the itemDescription
	 */
	public String getItemDescription() {
		return itemDescription;
	}

	/**
	 * @param itemDescription the itemDescription to set
	 */
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	
}
