package com.bazaar;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.utils.ParseUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class FacebookActivity extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.facebook);
		LogInCallback logInCallback = new LogInCallback() {
			  @Override
			  public void done(ParseUser user, ParseException err) {
			    if (user == null) {
			    	Log.w("com.purchace","User facebook login failed");
			    } else if (user.isNew()) {
			    	Log.d("com.purchace", "User signed up and logged in through Facebook!");
			    	Intent accountIntent = new Intent();
					accountIntent.setClass(FacebookActivity.this, MainFragmentTabs.class);
					accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(accountIntent);
			    } else {
			    	Log.d("com.purchace", "User logged in through Facebook!");
			    	Intent accountIntent = new Intent();
					accountIntent.setClass(FacebookActivity.this, MainFragmentTabs.class);
					accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(accountIntent);
			    }
			  }
		};
		ParseUtils.signInThroughFacebook(this, logInCallback);
	  }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}
}
