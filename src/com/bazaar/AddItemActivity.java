package com.bazaar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class AddItemActivity extends Activity {
	
	private Bitmap bitmap;
	private float price = 0;
	private String description = "";
	private static boolean onLeft = true;
	private static float leftHeight = 0;
	private static float rightHeight = 0;    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_item);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			bitmap = (Bitmap) extras.getParcelable("BitmapImage");
			if(leftHeight <= rightHeight) {
				leftHeight = leftHeight + bitmap.getHeight();
				onLeft = true;
			} else {
				rightHeight = rightHeight + bitmap.getHeight();
				onLeft = false;
			}
		    ImageView itemImage = (ImageView) findViewById(R.id.newItemImage); 
		    itemImage.setImageBitmap(bitmap);
		}	
		
		ImageButton addItemButton = (ImageButton) findViewById(R.id.addItemButton);
		
		Typeface type = Typeface.createFromAsset(getAssets(),"fonts/pirulen.ttf"); 

    	EditText priceText = (EditText) findViewById(R.id.itemPrice);
    	EditText descText = (EditText) findViewById(R.id.itemDescription);
		TextView dur = (TextView) findViewById(R.id.auctionDuration);
		TextView hr = (TextView) findViewById(R.id.textRight);
		TextView min = (TextView) findViewById(R.id.textLeft);
	
		priceText.setTypeface(type);
		descText.setTypeface(type);
		dur.setTypeface(type);
		min.setTypeface(type);
		hr.setTypeface(type);
		
		addItemButton.bringToFront();
		addItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	EditText priceText = (EditText) findViewById(R.id.itemPrice);
            	EditText descText = (EditText) findViewById(R.id.itemDescription);
            	
            	String pText = priceText.getText().toString();
               	if (pText.equals("") ) {
               		Log.w("com.bazaar", "Empty String!!");
               		price = (float) 0.00;
               	} else {
               		price = Float.parseFloat(pText);
               	}
               			          	
            	
            	if(descText != null) {
            		description = descText.getText().toString();
            	}
            	
            	if(onLeft) {
            		ItemsActivity.leftItems.add(new ItemData(bitmap, price, description));
            	} else {
            		ItemsActivity.rightItems.add(new ItemData(bitmap, price, description));       		
            	}
            	
            	//Intent cartItem = new Intent(AddItemActivity.this, ItemsActivity.class);
            	//startActivity(cartItem);
				Intent accountIntent = new Intent();
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				accountIntent.setClass(AddItemActivity.this, HomeScreenActivity.class);
				startActivity(accountIntent);	
            }
        });	
	}
}
