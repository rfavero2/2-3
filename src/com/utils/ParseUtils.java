package com.utils;

import android.app.Activity;
import android.content.Context;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;


public class ParseUtils {	
	
	private static Context context;
	
	public static void initialize(Context context) { //, String appId) {
		Parse.initialize(context, "0kUXYuEdUS9IW7k6OokJkmPk2JujLkwe9M1VVOW7", "VSeYSbJN4OHC9JEFTM5XnsdRKJnPpZ3cMBuWY9aW");
		
		// Initialize Parse facebook.  
		// TODO: Understand implications of doing this regardless versus doing it as needed
		//ParseFacebookUtils.initialize(appId);
		
		// Subscribe to default channel
		//PushService.subscribe(context, "", LauncherActivity.class);
		//PushService.setDefaultPushCallback(context, LauncherActivity.class);
	}

	public static void initializeAfterLogin(Context context, ParseUser user) {
		//PushService.subscribe(context, getLinkUserChannel(user), LinkUserNotificationActivity.class);
	}
	
	public static void createParseInitialization(boolean accountType) {
        final ParseInstallation deviceInstallation = ParseInstallation.getCurrentInstallation();
        deviceInstallation.put("parentInstallation", accountType);
        deviceInstallation.saveEventually();
	}
	
	public static void signUp(String username, String password, 
			String email, SignUpCallback callback) {
		ParseUser user = new ParseUser();
		user.setUsername(username);
		user.setPassword(password);
		user.setEmail(email);
		user.signUpInBackground(callback);
	}

	
	public static void signIn(String username, String password, LogInCallback loginCallback) {
		ParseUser.logInInBackground(username, password, loginCallback);
	}
	
	
	public static void findUserByEmail(String email, FindCallback findCallback) {
		ParseQuery query = ParseUser.getQuery();
		query.whereEqualTo("email", email);
		query.findInBackground(findCallback);
	}
	
	
	public static void findUserByUsername(String username, FindCallback findCallback) {
		ParseQuery query = ParseUser.getQuery();
		query.whereEqualTo("username", username);
		query.findInBackground(findCallback);
	}
	
	public static void signInThroughFacebook(Activity activity, LogInCallback logInCallback) {
		ParseFacebookUtils.logIn(activity, logInCallback);
	}
	
	public static void setContext(Context ctx) {
		context = ctx;
		
	}
	
	public static Context get23Context() {
		return context;
	}
}
